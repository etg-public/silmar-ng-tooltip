import {Component} from '@angular/core';

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.scss']
})
export class AppComponent {
  title = 'silmar-ng-tooltip';

  txt = {
    install     : 'yarn add @silmar/ng-tooltip @angular/cdk',
    module      : `import {NgTooltipModule} from '@silmar/ng-tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        BrowserAnimationsModule, // or NoopAnimationsModule if you do not animations
        NgTooltipModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
    scss        : `@import '~@angular/cdk/overlay-prebuilt.css';`,
    ex0         : `<button siTooltip="Tooltip with simple plain text">Simple Text</button>`,
    ex1         : `
    <button siTooltip="Tooltip with simple plain text">Simple Text</button>
    <button siTooltip="Tooltip <b>with</b> HTML text">HTML Text</button>
    <button siTooltip="Tooltip <b>with</b> HTML and light theme" [light]="true" maxWidth="100px">Light Theme</button>
    `,
    ex2         : `
    <button siTooltip="Top tooltip" placement="top">Top</button>
    <button siTooltip="Top Start tooltip" placement="top-start">Top Start</button>
    <button siTooltip="Top End tooltip" placement="top-end">Top End</button>
    <button siTooltip="Right tooltip" placement="right">Right</button>
    <button siTooltip="Bottom tooltip" placement="bottom">Bottom</button>
    <button siTooltip="Bottom Start tooltip" placement="bottom-start">Bottom Start</button>
    <button siTooltip="Bottom End tooltip" placement="bottom-end">Bottom End</button>
    <button siTooltip="Left tooltip" placement="left">Left</button>
    `,
    ex22        : `
    <button siTooltip="Tooltip text (click)" [clickable]="true">Will show only on click</button>
    <button siTooltip="Tooltip text (sticky)" [sticky]="true">Sticky tooltip</button>
    <button siTooltip="Tooltip text (sticky)+(clcik)" [sticky]="true" [clickable]="true">Sticky + click</button>
    `,
    ex3         : `
    <button siTooltip [content]="tpl1">Custom template 1</button>
    <button siTooltip [content]="tpl2">Custom template 2</button>
    <button siTooltip [content]="tpl3" [contentContext]="contentContext">Custom template + context</button>
    <ng-template #tpl1>
      <div style="width: 150px">
        <h4>Some title</h4>
        <p>Some text that can be anything really!</p>
      </div>
    </ng-template>
    <ng-template #tpl2>
      <img alt="test" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFRUXGBgaGBcXFhUVFxsYGBgYGBoYHRgYHSggGBolGxcXITEhJSkrLi4uGB8zODMsNygvLisBCgoKDg0OGxAQGi0lHyUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALoBDgMBIgACEQEDEQH/xAAbAAADAQEBAQEAAAAAAAAAAAAEBQYDAgEHAP/EAD8QAAECBAQEBAUCBgEDAwUAAAECEQADITEEBRJBIlFhcQYTgZEyobHB8NHhFCNCUmJy8QczglOSshUWJDRD/8QAGQEAAwEBAQAAAAAAAAAAAAAAAQIDAAQF/8QAJBEAAgIDAAICAgMBAAAAAAAAAAECEQMhMRIiBEETUTJx8EL/2gAMAwEAAhEDEQA/AJDK5Glbp+FQPvAePQUzpjD4mMLcmzBYnJBPCosR3g3xZPVKmy1p3SR7f8xwKD8qZCtlRODpT2P0iTGFmkBz2BirlL1SpauYHzERObZhMK9QPwqIp0MCKb4aJqcrmkukexj1OEmy3UQRzg6d4kKkpEpLECr84zT4hnh7KG4aNeT7QTLA5iUAkV6Q1w2cy1sVhKX93ic1EHUAwO0bYYpL0eDKEeswzzCYmahSUUUDvYxOYkFSQDttDmWlKgXVoO0c4hMtaC5GpO43hoWjICw2HlgDcmKXJmAKRsfrElg8UBMCmoNofZRjtc9VGBTT0gTiwSQzzOUkoOqwq46RNLwySol9Y2MVmKRqSRzBERmAxehRSqzwIX46BHgbJwQNgwgpGCCVA3Ec4bGoRMCT8Kkg+7wwx+KlylBJqlQoRtE5ed0MzDGES1ocUUQ0O8LK1FrUiQmJWqYmpKQoEdni5w0vTLc3ue20Fx8ULIkvFaFzJoSkcEsN6m5icxOEWkvpLRWZlNWlSikPzidxGPWu9Byi2KTrQ0RWhZeK3wvwkufiDt2/5id8sbiD8pHlzpano7e9PvFJ+yDLZaCF2d4hCQlMwcKifcQyMKfFEjVKBAqFD50+4jmS9hF08yiUHVMdwKJ+8ceK16ZnmpHCtKVAbWr8/rG82SZcgS030/M3Mc5xg1TMFLI+KWA/VNj9j6Qya8rCgXLiC6gGcB45zqWSkNGWTzv6TdoZT0uB0hm6ZvsbYf4Y4xtvURph/h/OUcYwcPtHMhTjDXhgocJ7faAMLeGRFD2ijMLZAvHhjqTYxzEoGISdNTqQtBqCCR2MUPjSW8lCxsr5ERJS5bRaY4ebgX30g+ojtnqUWUeqGmVL1YWUeifo0ROZlpkxH+aj83iv8NKfBo6fYmJXxFKbEKPV/kIXH/JgXRfLKwXDQRg5h0lW4vGcwCmk3jhGHXWt4q6Yw1y+aJjgjtHicH5ZKkF+YgfDKUgggVgpax8XOJPT0KYqkFSS4Li0AmUTs0UMpWkBbODGs+UmYl0j5QqyUaxHJkgJNHMM8tQAtJFP3jPLsOCpnboYIMgJVqKt4WU9gHyxHz3OUaJ6x1f3rH0J3DxFeLpLTgr+5P0pFML9qNj6CZqTqT0ly/8A4g/eOP4l0sq4tHucK/mqHIJHslIjGXK57xelSscqvCSfOPF8Mup7CKabM1JKuZ+QoIQeWcNJlyE0mTVJXMO4GyfzkYdLLcPIARyzdskyUzXHKlz1NUMKQpKgVOaPDHxDKecewhXjGo0VglRRBCS5a8a4fCkrSBuR9YFy48Tk0h/kMxJK5u0oE+thGnaA9FCsN6Uj8pIIr+NA2WkmSl7sCe94KTEHoQlvEOa6V6BsKw48J4vzZBCqsSD2MRebv5y9V9RHzh34GnstaOYBHp/zFp41+Mo1SHCcIEk0rWsDZioiWT2hjmOKEtYSoOmZY8lQFjBwK7ROPBBtgjwJ7D6CPcWOGOMsU8tB/wAR9BGuI+H0iH2AywYrDJVj2hdgRWGat+0OzCuX8MeNHSfgj9E4GIVamoU3ipyMBWGKP9h71+8JJKCaqtYQ28LukzEHoRHRklaHfA7wyn/8dSeRP2MJPEKAJinDuB9IeeHHeeg7KLdiIReLAQtKnukfImDHcgLoDh8GkDUKxhhZhKykiM8DmBQeYjqdmLzNaEsWaKeMrY2wyZl0wnhPpvBMjCnQrWLCA8mxKjOdamEUc3GpUOEA8+sTk2nTAyawmaFKChnD/KGRx6aBKmcRhisJKWSNKkK7RgMr00d+sFuHTaMcfmCtfDTtG2DzB0KQvfePf/oijxJL9I5l4U6Swcw/q1oOipy5Ty070+kIfF8l/LP+Wn3hvkdJYHIwZOykzklRS6ZfGbgHTYOOZb0eJRdTFivakTeXZRJnGbOnTFhpqkJSgJJ4QCSSbCobsYa4Tw8gTBO1hUmWCsghlApsCNxu/SKfw3KlJlqSmWJayeLTXVUkHUouLkbtG2JmIvxD+lyxFX5ilb94pOV6R3LAnHZBYaeqfiQpVirV2SKgewEPgtyTzjWbgQk6ktqLigrxHkBW/ePP4dQDs4sW59jCNXw5svx5xJHxBimnt0TAWY6HTpq8d+KP/wBjulMYZVI1KiyVRTFqkeKw6mpWHWCQZeDaypupR7A6E/NzGcmQolkpJ9Ie5hly9VEnSkIHTSmpPuYm5N6FZvgqcPJvo0bogXCmsFRHqEI/xBgx56jzAPyb7QPkixKxEsvcsexpFurw+jEuSSlSQGI5F4TZr4NmS2UhQWRUbGkdMbcCqVob55h9Ut90EKHpf5PAE5LpUOhh1h1akAndNR13hXOlsSI5sb+iRvk//aQP8RBOI+H0jHL0skD83jeb8MTfTGeX3PpDJW8LsBvDA7wzMLQOERyuNSKCMploSPAk5NxYSlOocMH5XjJZmpCC7pL/AFiexOIDLlLppJAMCZFidE+V/sAfWkdf4rTY/jZfZYjTi5w/vQlX1EIfGks/y/8AyHzEUYS2Jlq/uQpPsxhT4ySyAr+1Z+YieN+yFXSGUCI7kqDF7wwnTAwU0CT0pNQGjruxzJU6PEziLKIjhJrBmHwYUd/SC6CMMo8SLknjSFjrFHLx8jFDUElCh7GMsi8JhdWcczX5RWYTJpSGDPUCoUe9BfewhXji9DfivbJuXLCS4cDs/wBIGnZepSitCktyJZR2om5i1TgkA6lBOhvhHxFiSDUEC28cTZqQSClPQlKySCC7p96wFiUeDLHEncrwqgSFX5Xi2kZeUpCEoJ/uIYOP6u525UhJJlyxOQtwSakkhIFPiFY0wWMIVNU6vOQ4CX4SlT1IcfN9oyjGO2PDH4u0D57g5shQUSkyhZepXsw3eM8tx0qYClRIBb0b62gv+I8xKpM4KSFEKBFag1Fbgijx+w2VYRIYzNBJ/wAT+ftEnKL2dHjJA2NwRlhw62LJZhzb5D5iBpWPlqISQpB5Ko/ID1f3g3MPD2KQQvDzxMSElTJYkpAeg3bl3hOnxeFeWichc4UE5MxMpgbEyigBQIoQ/wC8Msfkm0xXkrTOMxwUlagtcsKvaj1fblWKPLMgwoQlUtAYjlUdD1EJJUsaNaCVIc6aklnLAnnaGWRYw6ikHSFVbYKF/cfSDjnT8WJkgntDleBShJISBsKc6QuzxekLA2lpT7q/aDsSC4ck1+kJM5nHSvqpI/8AaP3hsj0zmkKMNeClR5lOHMyYlI3I+VTBOZSCiYpJ2P1r9DHKlqyNasJyNf8AMb+4Ee1YaY/BkJUrkIn8JN0rSrkYfTseVIazvHRgdxotieicymcVBQNwo/Ov3jnMZdQfSMsvUUzVpahqIPxiHSfeOWfrkJS6DZfYP1+pjeZaMMBb3+sETBQwj6KZ5cIPH57wBl94YpFfznBfTAEyMViCJwjEiFhwxF53JSZmr+8BXuISqGhYPIg+xh9mGFMyTLUm6XT+kLsHkOImqYS1dyCBHowei0S9nlzh5g2WH7KSR9xAvjaQVSlBNwpJ+0NstwakoRLX8QAHqBBM7DJXMCV2UKxyRtSRNdPk3mqA0EQThsmxEwAplqY86R9gwuRSEBxLSfSCEsP6QBHckW8T5thvAU1aXdjDPAeFfKWkKUCpn5Boulks0IsRiWmLB1O4S1thsO/zgNUPGOxxLnIlJZVtxQGxr0/OUJsdmsqWotxFmHxEht3J/KQlzTFqfhNCWF3NqVil8J+EFKT501iltQuH5VNW+sFcKSYony8bif8AtoXUGpBS4pQbbCDB4DzJaXMxAIFASSr3AhvnX/UISHk4eWnhcFWxPTp3iYm/9QcXMZtKWuQ79ncRmmwXRplGBnyyUzkKBR/d8LAmyh6/vWCsQlSVKmjWQsgFzWlXdqV6bbwBhPEkxQUg13fmDWzubc+cbYWe/CpQd/gPf2ajxCTe0URTIkhaApgTuLG12u8ZLyhCy7Bj0Yg7ud6+0fsNOFixDDYWbn+nP2LlLUC4HCRydunTflHHtHUmmKcRlk6Xo/h16FpLg2Td61Nb/tGkqfMqqbhcMpRopQlpCiS4c1u+7Q0VihqcOVK2o46dd+kCYjNJEokrKQeRABvyFd4pGUlwnJRZLYjzVKeWgISGomovQtyvzg7BiUTVehZY3DPz+kYYzOzNnhMlIA/vVw1PIDt8zBkvK5oUlahKUkVU6UrNNykqc05Vi0Yt9JOS+ihw6VL0FTBQSXaxLs46FniOztbzFjYKLfnpH0CTKWuWZqZekkaUoPBQPpIejEEG9Ii8V4axjl5KjWpBSRXq8NlTrhx5L4beEJXGpbfCGHcx54iQfNc7j6U/SHfh3CKkyiJidKiag+wgfxWykJUGdJb0I/YRnGsYPH0JmHsxKPLC9yBTvCJMPMsSPKCzUh0gcvx4nhe2hcb2TWZzjLnS6MFFj2NIcG0IPGanSlSbpV9IdYSbqQlXMA+4hPkxppgyLYPh0aSR1MbL3j8tNXj1e8RuxDPLxUwwTeAsAKnvBgvBfQgc+8YpEb4i8YpjR4YanASpJllKAxLEfQwyM0AcIAjLHYVRlncpr7QVg5NASzEBo9NadHQuinEqImJV2+Uczw0xHdvnB2eSGCVekBZhdJ6pPuBHHk1N/wBkpdHKk9Y9lSaOYL8sMHPtHRkhuVI7ixx5Pr2EfPMzSpE2YSD8T86XEfSEz2FGI5x84zmfqxE4k/1bvZusJPhTH0/eGcvOLxIH9Cb9oaeMvEswq/hpaylCU6SE0c9WuNm5RnlGPTg8v8xP/cmO5LPxOAB6CJvJVGZMMwubmu53jKS7+jNO6GuTZCZheYC12ZzV69LbxSIyWTfQCqrABgD9wLRhhcSUsb0DtSmzN1eGaJjglO5u9XaOec22WjBISfwPl6mTo0sCWclXSnO/SE+OV5b6eZZQLhzWtOcXqsRQBSGS3Kt7fWF8/LZMyqkADpS/P5wFLZmmRGGzdb2c/T39IpsqzstUGo2FtvtC7M8i0KJl8SaU3/eF6Zy5ZAYj3gyjGQsZNF7JnFQcAVuS1KwpzTAoKvMmFJVVgT+NCqRmUxbBCVFZswP5vFhkfhVZImYq1xLBuev6QsMUuBlNAHhrwlMnAzeFCD8II+Lvu0VuXeGila1zpgXbQlKdKUtzq6uxpSG0jEJAoGAs1KbUjczwR+jR2RxpHO5tgWKwhWQ5FDtb2jDMMHwaXUkG5DU7FTtDKWpNo0XLSQxD97Q9C2TGAwKXUlSytItrqXNfidiK2aA/EOTvLUJY1U+EVLioh+qSNYDMVJL7M0FSUJQKsHLPS8CUbVGPlh8N4of/AMVFw/CyvdjSN8LgpstJEyWtAJpqSpIJ9Y+tyQNo7mywoMajkQCIlHAou0xFGnZ8DzTBpLoJBv7wTl6NMtKeQaPoue+B8NMClIJlrNQxJS/VPLtEpmXh2fh06llKkOwUkk3s4IDRD5EJVwXImxUsRwqNTGSrxxImfsDcwX+8CYK5gxqwz6EDxdzA6oJxY4oGmQY8MfQ8CkBNeImFeCkunSS2hRHoDT5Q0wgUBZoFVhWnrSahaQr1FD9o9N/R0sFzgJMk6TYg/aEuJrLB5D6GKDE4IFC0gEFi31hAkPKPQn5j9o5c69rJ5FsocMoaEkkB0ip7RpMIb4hCrDlS5KAE2o8ay5Gk8Tl6R1LaRRPQWmcghtxEn4ywwTMTN2UGIb59/wBIoJijUBBH+QrAGY4dS0GWahjU82gNWh4umS3iWYlctCEUSAHbn0jDw4yeHe3RqmA54UDpUGKXB+sF5dNHbqbNvHPK0qLxSbsopUqalifhp1hnglBndqHnz/V/lCrCZtZKnPPct+GGMuat9QDDkfnE7/ZTX0ODPC3B9vT9IDky1BTM459I9ws8GjOQfTnDdGITpYiNFW6FnKkcYHLvMVpDWqTYQfisFh5STqSFHmoP8onZucrROSiXqAYqJBACgCwB6PXqxvDEY8Kqti4Yg1H/ABHZCKSOWTYww+Llyk/y0JD1dhGqMxCwFP8ArCCfKSKhVGYBz3Dc+0dysPMASUpcWANwK1Pyh7oWihwSSx1MqpIZJSydgXUXPWnaGKSkCpAiQkYqahTPR61JZzt2/SGZxRID2cF1B97cI9faDYKH6QksddNjtH5RR/6j1/uhdhpYWS57cn6vGicrUmomBNKjS/VxW8MA1mLUuYEIQSN1sNKd7u5JpQCPZ2HUKk8Kas4r3jDBz1pWULWkqvYp4TbetvrG+MnJIIUQRuAGfvWMY5E5StJBOlndqPRh13tDLzSzkED82j5+PEUxJIJo5ZRNg9OkFr8STEs7kEb2BBY/hhfJBocY3xAgzRJBUHbjZmuftHee4cTMNMAXq4XDMQ6ai1rRzkCkqlhZCCS/EBRnuHs8CZ7i5KUIWgfEvSSAA4ILuN7CsaS1QKsgoKRlUxYSqWkrd3CRUFN39xGE1IBIBcAkA9HpFR4CxgStcs/1Bxfa/wCdI8vDBSn4yOdLdE5JyiekkqkzAOekxsrCTA6ihQSDVRSWHrH1GZiwNvXaMsSAuWWZVDRqesdb+Iv2U8D5Hixxe0CzP1it8UeG1IabKDo08QFSlt61KfpEksRyuDg6YrVH0eUlQqT84yxs3SqXMtpUx7Kp9Wj2UeceY9AXLKQQ5HPfaPSa0XDJ04KoAbX7xFS0t5iTsfoSPvFBh9RSlfMDeE05LT5g5g/MPHNn2k/9sWf0H5Cs+Qw2UYLmsW1VMBeG8QlKZiVEDievURrjsfLSEkVL7WiuJ+qDHgWtZCXoAdxUxjNkIpoLvd4WrzHXQMnpC6fjC7Ak82h2xkIPFeE0YnzB8K6eoDN7QmlKqfz8pD/xTxynYkpUCCfpE7LmkRDItl8ctDCTiW36/eGIzEseJ3/PpE+qZTrGUvGaVOTEvCynkfT8knJ0uqzb7Qn8Q42ZMmvKWEoDV5nk3tCCXni1jy0JJTam+14NwmACgCuTpa5Cish7EDvFoQpEXJNjaWxKOFCiaFphKi3+JEPpOGZyA1WBNSDtaxhNlmC8snQmtAAE8ZS/IA03igwuEUptY0qJdmBLdh2BiiFbOESHu/8As7E+nKDZE+YE6bgmhAqB1PtBGGSQg6TergVJ9fT3jlOGUq6qEuxG42BowcVvDULZwtLmlQdmv7xkmb5aiCSBVgGZiAW5j5Vg+fpKRwvThsGLtRnY0jMyQQELlkizhQubGlfeNQDhGKYMlQA3UqocgWKQ4Nh+NGqcyWGEwFKja41duXa8cy5KZY4DxXuVEEuHueu4jTSVBlMrUKoIBKh7i5gpgFedmYtfACogF2YE7gfjRPS81nBZRN1BXJQPLfoecPpkmeiYlEsAuSy1FRFbBqBRHQ9d4fJyyWqs5KVqYB2It69TDVZrINUtK1NMmMSKJFxSNsLlGKUFJ0KEpmDqSlZerivD6/KLTGolBOlISkNYAM/3jXB4hYSFLQE0qxB+tY3igWRmHxU3DpTKVJmBIYFgpdDeooetYInypkxSNEsiUKigYbMxrYvFpLzWWpw7hntb1jmTmcqbMEsD/wAgH9HEbxs1kL4jy8SZgCQQlSQQ/Ox+0Y5BjPKxEtezsexoYr/+oOAHkomD+lTHsofqBEA7R52VfjzWQlpn13FYhKUuSw5kQgx+faFAJSa3tpDHk9YExuaom4VK5hNEi1OIUP51hUtUsup3G4Jr+fpHc3fDoTVFfk2MCgHYv+j1iOznwlM81Rw6QpBNBqAKelbiNMnz5QmeUCeIKAVsHBIp0MH5RmhspgsOFAVDgtE5uEn4szhZyJp3NG3gaZjVC7MBdoSzMepQpbu5EfhPGl1OfeH8h6N14k1SCGSSa0oawGie63jmbOSTa4t2jOWwIZLbxHL/AB/onJaPZmMMsks4N4wGMUtmT7x1i5BUwF/0gjC4V9rCtYOH+JocM0SqVMZqSRVnOwENMNh0Pf1P0gslDEEhNL2Yd9oskPZJ59P0ykhYbWqgF6V+rQpwuCM1aESwSVQ9zEy5yglHEwYLetaEsbdOdIb+GsjmS1eY4SQCAoi1CNXTlCyiGMjrPssw2DlaC87EKDJQkAAbFRo7RE//AG6tR1TSlL/0vaPokvK0g6zMJWX1EsSbvy/xalAISY5KdZAKrBzdy7s494bQDbwxgJSSRpckG1fvFZh8nQRqLvR2oAPpz53hTkU1KEMlOhVXU19nflaKHCpUASFlQF2A9rXjJmOGCSyQkAE1JqQKU/aOUpUTUsD1FaHe4J+kbnSpNfhFyK71ZjeFeLny5nDJFQWDJBcDYElh1PSMAaS5qQaAAdedd7UcdnjPEYkJUzFZLslJNL3rw9ObxjhVFQIMpaQHYqUmp3GlJvXpBScR5aRqHNkp3H6wTGuGQAhwhju7Dk4FDb7QGUrU+os5LUqKGndgKx5iMaqYCEOk7FtQehqOTM4p3jGTmOlRlrW6x8KQEgsL0dy3XnAZg3CYcqpp4QzEsz/1AjoXjXWUhQSgEPVRd3e4a8dYI0UFKLEgh+RAoB3rBCcQgKbUzNswdqB+zQQAU+QVgpUAp2oFFrXa4MT+bS1yyBKKphKhQFkh9y9g28VwxKHIfle1vdqx+C0hIDgNyF+/OMEnJalODPIChZCTqH+xJZ+0MpmLSQxUX2qD+CBc1wCZhdJZydqm3QARLz14iVqOlJSCw1ULel/lGs1FBPw4VRCQVGweKzByAmQlNAUpADMzjcR84y7xFoWVTEsGLAekbI8XhQZUw32SpPoQ20bzo3iPPE2aLVKVKUAXFWe4q49REW8d5tmaSBpKlEkW/c/KMpanAMcHybdMlljWz3E4kiWEH4dQv8qb1gqVPUlXmAApq7GptRoFKQbh/wBdoZ+SUywBuXqzCm3WLfHlcBsbtGWGlDVLKSkMQVFy5c29OUO5EoBZUKE3bnAWXpDVbVy60guYkuz6T2hMjqabHcqYiVpDkF07hmLxli56dnI5P9OcczCvSSWANz9KRhPQCxKk9xf2jppDWwiUtJAI2P8AzHU+YHYENf8AaONPAGDc1Gj+kBT5wCkhh3hZq00IwnFr3G36PC2bmK3oPZwIZTqj2jfDZaJaStaqX0M4HR7QmDjFgYZYqfNIolKaOpVAl/Wp6CG87C8WkcRWbO5VSno/o3OMkZUVEBBZDVNgl+rekOZGCEoFKA6yzrep5127RcpQvyvKEYdJV8cyhZuEXra/0akOBjypAQUp6tQEi17AWjHFSFOSVFLM7et6UPaNMFgQ2oklNy9SQdi+3eBbCLcbg5ilhXmEhqp4aXoAOXd4Kw2BTVKdKVhr0BrytDHFIlIR5qw2ncJY1FyBsPpCrBYhU0lSTqSQDqBagozGop0jAN52CYBLgbgCm1RUfjwywiTpbYPSjk0bn84VrzVylIYB7VJrzMNpoIBANGepoKsaw0UBmExpjVYA3Af8PzguWpCUFhagYd29YUzcSJYGsuk/CdNQ+xH0O8aCehYOmYWN9P6eloAQhCgKuz8QD+w6V6RotYSkqNSGBete21O20Cy5YBIBJYaq8BH2PryMag2CiQCLsn0e7PAQD2eovqAvdmZrh33rGq0oAcswsEoL8nGm33jBGNla9IJJSRqCUk3qKpBD2peM0y1zljVL0JRVL1WbVvwD58xBMbIWuty5BqBwigaguWgqTh9SSSgpNaayktzDUPS3pHkuWlJJ3DVLW5MAAW5d4MCtYIBLswsPUXEMAWLxC5ZdSHGyw5OnmofELbc+kY4ZJWeBepINklISQX37w1nySlWtYK0kVSXcWDsk6dy9HjJWFKFaxpSKOBqZj1JvarbbwAnWXYZTETJjKGwCWA5ajfvSF+PyaXMJ/mKKm/qbSNmLbQes6qXB5PSlDH6ZIF0lOoEOWcM4JtXaMAjsb4XmOACk82qLUrSrtCjHeH5svXpUnUmpb4m5gW5UrH0jMk8CUqClORYsAxFTuzPE7iVkqBUAyKFVzQs7mhTXerQrQyZH+GsOZgICnUCdWogG92/SGWKwxlkAl3q/rWK9OWIVxp0JJIKlBAcgCg1bmt+kKvEWG06XYgOXFPRiXD0p0POOfND1sXJuIqWQmW39RIMOMGtKqKLEhwdJUDYtE8V6qmKvwikzEFNOFXIOxrHPhbTpC4WrphMjCaJskPrJJ6AU2ePfEcgomvTiANOdo3zmYET8M39x+jQV4ql6koUNiR7h/tHXmg5Y3+wT+z42jEkjSSw3jQTwj4SGvUOQYCxcgJUSCefRjAk6WTxIUz35Q60FOhyvOir4yDyv7x+GKCtKgl3JDM/WJyYktUuecUXgrATp4UiWAGIPmKfSnuRUlgaCCxmPMNp1OVJ4ahNz3LWilw2A1aFzAQD8KRwlRYly3whnpc9I0wOVycONQWFrA+MoDuNxslNxYnrHU2eXOg61KFNAdVVORtpHqPk8CMKGTpaMsVNWs6ZYoHoAkJDPQc23rBOWSUJBCluQQQXDhzy5PGImqEji1qW51FlAhTklKUgAlI2NbX3jXJ8OgMtQZbMwFWuKOWbkYcwzlS21agSGF6BSqvT2jso+Kwd7DpT8Ec4yaUgqsAbnq343SM5k0FLhTsP39TBAZlYWkJZtV9wC3MUuHftE3nwOGYyAAjcBwa3B5g394pdfCGDkih22D9naEkxXmBSVJoXClAOOTdIRvdGsn8sx2pYQrgdgCBck0DvaLzEYqUUrS5BlCgG7cxsf2j55neWaVAS0MNqgO26a1qXgXCZjP/7M00VxFUwFSy1uIKqIpBpKmZj+XiZ0wnhLKNiWb/FzRIbpBmCQrUTqBQSCxA2Fa77QtVikS0J4uJw9VU7hTAhj1gjC4rhoaf029u/aEbC+DmXN1MAqpeiQx+au1I4UFlnWoMwWAlL9KKBO13Y9IFlFSQCT/rRRrycML/lI9KpwIKtD8iFGgJccvXtAXALgVKSTRwhA+Gmn4bdztaM8Rma08KFhS0kEFmdLCpqAmgaj+sGrbSFJ+IuCpV2eqnLMADyhZjcvxJLy8RxBwdctLk76Shu1YJhllmaJW/mJUgtQaCGrUPv6Qxw2IBdOjVs9RTd2G7QhTi56EJ88Bax8JSTVq1S9S2zWg/K8zRNISCRdxqAUH2b2gmDMVmiZJTqUTq4QACwI5q29YExC/MVqoKnS6n9WFAWDfWOM0wEucNC0kuASSWb+0sNn+8FYTLpckJQkEgAM5dw1gIJjvUgJDubD4fffrG6ZoKiAp6U2Lc6W2Z468vgI1BzcgfUWG0cDDpZmD9XBLW2G20YBlmcrzZJTqUS9w1xUVBblsRE6FsooSRpFSb1BIIa6d2H6xSIwalpUVEIlUoDdjZ6e8LzlUp1EKIDggKS4c3o49j1jNBNsNOlrL1cWTcB9+9i0TfifH6leWmwv1MNJstMiWrWpK9JOhklDXOnq+x/R4m8rSJk9OssCXUelzHPlbfqieR/SMk/aHnhHGaJxTssN6io+8J8agImKSLBRbtt8o/YedoWlQ2IMccH4TJLTKbxTiXmyNmVc8y0WBw4WgAsbH5RBeKlHVLJ7g9L/AJ3ixweKJQmmk6Rc3+8enF9K/wDTPkOGyLF4mVLXKlkBVCpQ0gDnWpHaLjw9/wBO5MpP88qnLNyXSjsEg/WLCULQwaCooaq0SK/CeXyXV/DIJ2BBWfTUaRziloTRATQHhDAB2BoKGlIPxVdb14m9OXaJ7Jqy0E1JS5J3JN+8FjI1TiAVETaJAFEuFVs5B4X6s9Y1n4bSdctll6pBqzGiepp0LQJkBfzH/uSPRjTtDE8MxIFA6qCg/o29T7wDHeFBUl1o0pSp9PCT1UTcVBp3e8Gy8CQHlK0kVYsQQ1jy29o5xlCW3SPqIzWsuA5Zlhno1Y1hoHRlE1T+YssbsUP/ALBw+7dIPRgdABcrNiDsn0taOpqyE0JFOfeNcYfj7q+SmEZABMUsJAKBwiiRQCvzHtvCHLJpUqckFgVPZ+kP5dQt6/HeuxiRwB/mL6kv84SfUI+oa4vCylpAUgLZmLVpu4tCXG/w80BC06dIoocJHI/Md4HyTEL4uJXxNc21W7R5iEDzDQUCm/8Aa/1hhxdicDPl0QBMQASCLDetOT2jvI8SpylQCUgPQ8z19fnHmAnK/uNBSpo/LlHYoub1CfrCS4CXCplY2UUhCpqSVUTLS9TfiVYO3ygtEoMdMtKQKEC7gEu96UHV+lV2BAElBFDrSHFCxIcdukVE9IC5oFBqSGFAzmkPWgmeWSSrSqYXOydqsX7h2jLE45UslCUFZKjpZrAsxYECm8dZkshRYkMlLMY1wCAAGADqLtT+oQUBmOYYLWCsJKJhAZT6FjqNQsW5dIxxGXz9KSZUlShcglJPZxwm+59NnaPjX/pBQ+H0g0Ei05uqUVa5E1A1Cw8wFnvpoK9TGCPEcp9JSupfSZaypRvQAGz2+kN8OslRBJI1mnvCvEKIE9izKU3RnZuUBKzGp8RLUwRInA6m40aGfqxNbtWGuC0zCkqPF/ZrUGbmDU+vtG+XSkmSSUgk6XLBz35xti5KRZI9hzEFIAFmc/jTLSkvWrWtvtdm6wUUAEvdgadglu/D84ksfNUMQWUR2JG8U2osC9WvvtAsJI+K8UTM8tzw3oL7dbc468KSSCubo1gBmZzzp+sKs2P86Z/sYtfCAbCgihKlOY5YbyNkVuZOeKA80L0FGtIcEEVG9RyaFJtFX40H8uV/sqJPb1jmzqpsWa2HY+fqlSr0cEvy2Zqc4p8n0CUkoQAojiJWa+r3ttv0iKPw/wDl9orcoQP4dNBfl3jrwytDw6f/2Q==" width="250" />
      <h3>Chipmunk</h3>
    </ng-template>
    <ng-template #tpl3 let-data let-one="one">
      <div>Data: {{data}}</div>
      <div>One: {{one}}</div>
    </ng-template>
    `,
    scroll      : `
    <body>
      <div style="overflow: scroll" cdkScrollable>
        <p>Some paragraph that will have <span siTooltip="Tooltip text" [sticky]="true">tooltip</span></p>
      </div>
    </body>
    `,
    css         : `
    <style>
      ::ng-deep si-tooltip-cmp.custom-class {
        font-size: 2rem;
        border: 5px purple dashed;
      }
    </style>
    <button siTooltip="Custom CSS class" addClass="custom-class" [sticky]="true" [clickable]="true">Custom CSS class</button>
    `,
    long_content: `
    <button siTooltip="Lorem ipsum dolor sit amet..." maxWidth="250px">Max width: 250px</button>
    <button siTooltip="Lorem ipsum dolor sit amet..." maxHeight="100px" [sticky]="true">Max height: 100px</button>
    <button siTooltip="Lorem ipsum dolor sit amet..." maxHeight="150px" maxWidth="250px" [sticky]="true">Max width and height</button>
    `
  };

  contentContext = {
    $implicit: 'Implicit data',
    one      : 1
  };
}
