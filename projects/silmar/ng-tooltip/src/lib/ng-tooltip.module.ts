import { NgModule } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgTooltipComponent } from './ng-tooltip.component';
import { NgTooltipDirective } from './ng-tooltip.directive';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations : [ NgTooltipComponent, NgTooltipDirective ],
  imports      : [
    CommonModule,
    OverlayModule
  ],
  exports      : [ NgTooltipDirective ]
})
export class NgTooltipModule {
}
