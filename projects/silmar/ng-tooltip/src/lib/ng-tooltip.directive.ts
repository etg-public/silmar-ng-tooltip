import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  TemplateRef
} from '@angular/core';
import {
  ConnectedPosition,
  FlexibleConnectedPositionStrategy,
  Overlay,
  OverlayConfig,
  OverlayRef
} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {NgTooltipComponent} from './ng-tooltip.component';
import {
  POS_BOTTOM_C,
  POS_BOTTOM_E,
  POS_BOTTOM_S,
  POS_LEFT_C,
  POS_RIGHT_C,
  POS_TOP_C,
  POS_TOP_E,
  POS_TOP_S
} from './misc';
import {ESCAPE} from '@angular/cdk/keycodes';
import {Subscription} from 'rxjs';
import {normalizePassiveListenerOptions} from '@angular/cdk/platform';

const passiveListenerOptions = normalizePassiveListenerOptions({passive: true});

@Directive({
  selector: '[siTooltip]',
  exportAs: 'siTooltip',
})
export class NgTooltipDirective implements OnChanges, OnDestroy, AfterViewInit {
  /**
   * Use simple text
   */
  @Input('siTooltip') text: string | undefined;

  /**
   * Pass template instead of simple text string
   */
  @Input() content: TemplateRef<any>;

  /**
   * Pass template context
   */
  @Input() contentContext: object;

  /**
   * Maximum width of tooltip
   */
  @Input() maxWidth: string | undefined;

  @Input() maxHeight: string | undefined;

  /**
   * If the tooltip cannot be placed inside the viewport try to push it (this can place the tooltip over the attached element)
   */
  @Input() push = true;

  /**
   * Change the tooltip placement
   */
  @Input() placement: 'left' | 'top' | 'right' | 'bottom' | 'top-start' | 'top-end' | 'bottom-start' | 'bottom-end' = 'top';

  /**
   * Keep the tooltip open on mouse leave
   */
  @Input() sticky = false;

  /**
   * Open only on click
   */
  @Input() clickable = false;

  /**
   * Use light theme
   */
  @Input() light = false;

  /**
   * Set uniform padding
   */
  @Input() uniform = false;

  /**
   * Add arrow to the tooltip
   */
  @Input() arrow = true;

  /**
   * Disable the tooltip
   */
  @Input() tooltipDisable = false;

  @Input() addClass;

  protected overlayRef: OverlayRef;

  protected canClose = true;

  protected position: { [key: string]: ConnectedPosition[] } = {
    left          : [POS_LEFT_C, POS_RIGHT_C],
    right         : [POS_RIGHT_C, POS_LEFT_C],
    top           : [POS_TOP_C, POS_BOTTOM_C],
    'top-start'   : [POS_TOP_S, POS_TOP_E, POS_BOTTOM_S],
    'top-end'     : [POS_TOP_E, POS_TOP_S, POS_BOTTOM_E],
    bottom        : [POS_BOTTOM_C, POS_TOP_C],
    'bottom-start': [POS_BOTTOM_S, POS_BOTTOM_E, POS_TOP_S],
    'bottom-end'  : [POS_BOTTOM_E, POS_BOTTOM_S, POS_TOP_E],
  };

  protected subscrOutside: Subscription;

  protected subscrKeydown: Subscription;

  protected portal: ComponentPortal<NgTooltipComponent>;

  protected showTimeout: any;

  protected listeners: (readonly [string, EventListenerOrEventListenerObject])[] = [];

  constructor(protected overlay: Overlay, protected elRef: ElementRef) {
  }

  ngAfterViewInit() {
    this.listeners = [];

    this.listeners.push(['click', () => this.show(true)]);
    this.listeners.push(['mouseenter', () => this.show()]);
    this.listeners.push(['mouseleave', () => this.close()]);

    this.listeners.forEach(([event, listener]) => {
      this.elRef.nativeElement.addEventListener(event, listener, passiveListenerOptions);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.overlayRef && (changes.placement || changes.push)) {
      this.updateStrategy();
    }

    if (changes.tooltipDisable?.currentValue) {
      this.close(true);
    }
  }

  ngOnDestroy() {
    this.close(true);

    this.portal && this.portal.isAttached && this.portal.detach();
    this.subscrKeydown && this.subscrKeydown.unsubscribe();
    this.subscrOutside && this.subscrOutside.unsubscribe();
    this.overlayRef && this.overlayRef.dispose();

    this.listeners.forEach(([event, listener]) => {
      this.elRef.nativeElement.removeEventListener(event, listener, passiveListenerOptions);
    });
    this.listeners.length = 0;
  }

  /**
   *
   * @param clicked
   */
  show(clicked = false) {
    this.canClose = clicked ? !this.sticky : this.canClose;

    if (!this.isOpen() && (clicked || !this.clickable)) {
      clearTimeout(this.showTimeout);
      this.showTimeout = setTimeout(() => this.open(), 50);
    }
  }

  /**
   *
   * @param force
   * @protected
   */
  close(force = false) {
    clearTimeout(this.showTimeout);

    if ((this.canClose || force) && this.isOpen()) {
      this.canClose = true;
      this.overlayRef.detach();
    }
  }

  /**
   *
   * @protected
   */
  protected open() {
    if (this.tooltipDisable || this.isOpen() || (!this.text && !this.content)) {
      return;
    }

    this.createOverlay();

    this.portal = this.portal || new ComponentPortal(NgTooltipComponent);
    const ref   = this.overlayRef.attach(this.portal);

    // Pass content to tooltip component instance
    if (this.content) {
      ref.instance.content        = this.content;
      ref.instance.contentContext = this.contentContext;
    } else {
      ref.instance.text = this.text;
    }

    ref.instance.light     = this.light;
    ref.instance.uniform   = this.uniform;
    ref.instance.arrow     = this.arrow;

    this.addClass && (ref.instance.elRef.nativeElement as HTMLElement).classList.add(this.addClass);

    setTimeout(() => this.overlayRef.updatePosition(), 100);
  }

  /**
   *
   * @protected
   */
  protected createOverlay(): OverlayRef {
    if (!this.overlayRef) {
      this.overlayRef = this.overlay.create(new OverlayConfig({
        positionStrategy: this.createPositionStrategy(),
        scrollStrategy  : this.overlay.scrollStrategies.reposition({autoClose: true, scrollThrottle: 10}),
        maxWidth        : this.maxWidth,
        maxHeight       : this.maxHeight,
      }));

      this.subscrKeydown = this.overlayRef.keydownEvents().subscribe((event: KeyboardEvent) => {
        if (event.keyCode === ESCAPE) {
          event.preventDefault();
          this.close(true);
        }
      });
      this.subscrOutside = this.overlayRef.outsidePointerEvents().subscribe((event: MouseEvent) => {
        const target = event.target || event.relatedTarget;

        if (!target || (target !== this.elRef.nativeElement && !this.elRef.nativeElement.contains(target))) {
          this.close(true);
        }
      });
    }

    return this.overlayRef;
  }

  /**
   *
   * @protected
   */
  protected isOpen() {
    return this.overlayRef && this.overlayRef.hasAttached();
  }

  /**
   *
   * @protected
   */
  protected createPositionStrategy(): FlexibleConnectedPositionStrategy {
    const positionStrategy = this.overlay.position()
                                 .flexibleConnectedTo(this.elRef)
                                 .withLockedPosition(false)
                                 .withFlexibleDimensions(false)
                                 .withViewportMargin(5);

    this.updateStrategy(positionStrategy);

    return positionStrategy;
  }

  /**
   *
   * @protected
   */
  protected updateStrategy(strategy?: FlexibleConnectedPositionStrategy) {
    strategy = strategy ?
               strategy :
               (this.overlayRef.getConfig().positionStrategy as FlexibleConnectedPositionStrategy);

    strategy.withPositions(this.position[this.placement])
            .withPush(this.push);

    if (this.overlayRef) {
      this.overlayRef.updatePositionStrategy(strategy);
    }
  }
}
