import {
  ChangeDetectionStrategy,
  Component, ElementRef,
  HostBinding,
  Input,
  TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector       : 'si-tooltip-cmp',
  template       : `
      <div *ngIf="arrow" class="si-tl-arrow"></div>
      <div class="si-tooltip-inner">
          <ng-container *ngTemplateOutlet="content ? content : defTpl; context: contentContext"></ng-container>
          <ng-template #defTpl>
              <div [innerHTML]="text"></div>
          </ng-template>
      </div>
  `,
  animations     : [
    trigger('tooltip', [
      transition(':enter', [
        style({opacity: 0}),
        animate('150ms cubic-bezier(0, 0, 0.2, 1)', style({opacity: 1})),
      ]),
      transition(':leave', [
        animate('100ms cubic-bezier(0, 0, 0.2, 1)', style({opacity: 0})),
      ]),
    ]),
  ],
  styleUrls      : ['ng-tooltip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation  : ViewEncapsulation.None,
})
export class NgTooltipComponent {
  @HostBinding('@tooltip') readonly animations = true;

  @HostBinding('class.si-tooltip') readonly mainClass = true;

  @HostBinding('class.si-tl-light') light = false;

  @HostBinding('class.si-tl-uniform') uniform = false;

  @Input() arrow = true;

  @Input() text: string;

  @Input() content: TemplateRef<any>;

  @Input() contentContext: object = {};

  constructor(public elRef: ElementRef) {
  }
}
