# NG Tooltip

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-tooltip.svg)](https://www.npmjs.com/package/@silmar/ng-tooltip)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-tooltip/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-tooltip/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-tooltip.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-tooltip)

### Install
```
npm i @silmar/ng-tooltip
// or
yarn add @silmar/ng-tooltip
```

### Demo
[Checkout the demo page](https://etg-public.gitlab.io/silmar-ng-tooltip) or run the demo app, check out the project then run the following in th root directory

```
npm i
ng serve
```

More info in the library [README.md](projects/silmar/ng-tooltip/README.md)

